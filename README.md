# How to build the project.

Run from powershell:

* `mkdir build`
* `cd .\build`
* `cmake .. -G "Visual Studio 16 2019"`
* `cmake --build . --target ALL_BUILD --config RelWithDebInfo`