#include "utils.h"

#include <locale>
#include <sstream>

std::string Utils::WideToString(const std::wstring& data)
{
	static std::locale loc("");
	auto& facet =
		std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(loc);
	return std::wstring_convert<std::remove_reference<decltype(facet)>::type, wchar_t>(&facet).to_bytes(data);
}

std::wstring Utils::StringToWide(const std::string& data)
{
	static std::locale loc("");
	auto& facet = std::use_facet<std::codecvt<wchar_t, char, std::mbstate_t>>(loc);
	return std::wstring_convert<std::remove_reference<decltype(facet)>::type, wchar_t>(&facet).from_bytes(data);
}
