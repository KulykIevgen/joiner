#pragma once

#include <string>

namespace Utils
{
    std::string WideToString(const std::wstring& data);
    std::wstring StringToWide(const std::string& data);
}
